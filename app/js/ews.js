$(document).ready(function() {
	$(document).on('click', '#toggle-menu', function(){
		$(this).closest('header').toggleClass('menu-opened');
	});

	$(document).on('click', '#show-user', function(e){
		e.preventDefault();
		$(this).closest('header').toggleClass('user-menu-opened');
	});

	$(document).on('mousedown touchstart', '#show-password', function(){
		$(this).closest('.input-show').find('input').attr('type', 'text');
	});

	$(document).on('mouseup touchend', '#show-password', function(){
		$(this).closest('.input-show').find('input').attr('type', 'password');
	});

	$(document).on('click', '.show-options i', function(){
		var thisItem = $(this).closest('.show-options');
		if (!thisItem.hasClass('opened')) {
			$('.show-options.opened').find('.inside-box').slideToggle('normal');
			$('.show-options.opened').toggleClass('opened');
		}
		$('.cart-item.show-fields').removeClass('show-fields');
		$('.cart-item.show-promo').removeClass('show-promo');
		$('.cart-item.show-comment').removeClass('show-comment');
		thisItem.toggleClass('opened');
		thisItem.find('.inside-box').slideToggle('normal');
	});

	$(document).on('click', '#buyer .visibled', function(){
		var thisItem = $(this).closest('.drop-down');
		thisItem.toggleClass('opened');
		thisItem.find('.inside-box').slideToggle('normal');
	});

	$(document).on('click', '.change-item', function(e){
		e.preventDefault();
		$(this).closest('.cart-item').addClass('show-fields');
		$('.show-options.opened').find('.inside-box').slideToggle('normal');
		$('.show-options.opened').toggleClass('opened');
	});

	$(document).on('click', '.promo-code', function(e){
		e.preventDefault();
		if ($(this).closest('.cart-item').hasClass('show-comment')) {
			$(this).closest('.cart-item').removeClass('show-comment');
		}
		$(this).closest('.cart-item').addClass('show-promo');
		$('.show-options.opened').find('.inside-box').slideToggle('normal');
		$('.show-options.opened').toggleClass('opened');
	});

	$(document).on('click', '.comment', function(e){
		e.preventDefault();
		if ($(this).closest('.cart-item').hasClass('show-promo')) {
			$(this).closest('.cart-item').removeClass('show-promo');
		}
		$(this).closest('.cart-item').addClass('show-comment');
		$('.show-options.opened').find('.inside-box').slideToggle('normal');
		$('.show-options.opened').toggleClass('opened');
	});

	$(document).on('click', '.cart-item .discard, .cart-item .save', function(e){
		$(this).closest('.cart-item').removeClass('show-fields');
		$(this).closest('.cart-item').removeClass('show-promo');
		$(this).closest('.cart-item').removeClass('show-comment');
		$('.show-options.opened').find('.inside-box').slideToggle('normal');
		$('.show-options.opened').toggleClass('opened');
	});

	$(document).on('click', '.collapsed', function(){
		$(this).closest('.card').find('.collapse').slideToggle('normal');
	});

	$(document).on('click', '.category-accordion .category-title', function(){
		$(this).closest('.category-accordion').toggleClass('opened');
	});

	if ($('.category-slider .slider').length) {
		$('.category-slider .slider').slick({
			// arrows: false,
			infinite: true,
			speed: 300,
			slidesToScroll: 2,
			slidesToShow: 1,
			variableWidth: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
		});
	}

	if ($('.insta-slider').length) {
		$('.insta-slider').slick({
			// arrows: false,
			infinite: true,
			speed: 300,
			slidesToScroll: 2,
			slidesToShow: 1,
			variableWidth: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
		});
	}
});